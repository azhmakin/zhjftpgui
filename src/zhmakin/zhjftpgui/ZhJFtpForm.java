//
// Copyright (c) 2018 Andrey Zhmakin
//

package zhmakin.zhjftpgui;

import zhmakin.jftpserv.Credentials;
import zhmakin.jftpserv.JFtpServer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


/**
 * @author Andrey Zhmakin
 */
public class ZhJFtpForm
{
    JPanel pnlMain;
    private JTextField txtRoot;
    private JButton cmdBrowseRoot;
    JButton cmdStart;
    private JSpinner spnPort;
    private JCheckBox chkAnonymousReadOnly;
    private JCheckBox chkAnonymousReadWrite;
    private JTable tblUsers;
    private JList lstLog;

    private ServerThread serverThread;


    public ZhJFtpForm()
    {
        this.initUI();
    }


    private void createUIComponents()
    {
        // TODO: place custom component creation code here
    }


    private void initUI()
    {
        cmdStart.addActionListener(e -> this.startOrStop());

        cmdBrowseRoot.addActionListener(e ->
        {
            JFileChooser fc = new JFileChooser();

            fc.setCurrentDirectory(new java.io.File("."));
            fc.setDialogTitle("Choose directory");
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            if (fc.showOpenDialog(ZhJFtpForm.this.pnlMain) == JFileChooser.APPROVE_OPTION)
            {
                txtRoot.setText(fc.getSelectedFile().getAbsolutePath());
            }
        });

        this.spnPort.setModel(new SpinnerNumberModel(21, 0, 65535, 1));


        this.chkAnonymousReadWrite.setEnabled(this.chkAnonymousReadOnly.isSelected());

        this.chkAnonymousReadOnly.addChangeListener(e -> {
                chkAnonymousReadWrite.setEnabled(chkAnonymousReadOnly.isSelected());
            }
        );

        DefaultTableModel tableModel
            = new DefaultTableModel()
            {
                private static final long serialVersionUID = 1L;

                final String[] COLUMN_NAMES = {"Username", "Password", "Write Access"};

                @Override
                public int getColumnCount()
                {
                    return COLUMN_NAMES.length;
                }

                @Override
                public boolean isCellEditable(int row, int col)
                {
                    return true;
                }

                @Override
                public Class<?> getColumnClass(int columnIndex)
                {
                    switch (columnIndex)
                    {
                        case 0:
                        case 1: return String.class;
                        case 2: return Boolean.class;
                    }

                    return Object.class;
                }

                @Override
                public String getColumnName(int index)
                {
                    return COLUMN_NAMES[index];
                }
            };

        this.tblUsers.setModel(tableModel);
        this.tblUsers.setDragEnabled(false);

        this.lstLog.setModel(new DefaultListModel<String>());

        /*tableModel.addRow(new Object[] { "andy", "PassWord", false });
        tableModel.addRow(new Object[] { "john", "P@ssWord", false });
        tableModel.addRow(new Object[] { "hugh", "P@ssW0rd", true  });*/

        tableModel.addRow(new Object[] { "", "", false });

        /*addLineToLog("Starting ZhJFtpGui");

        for (int i = 20; --i >= 0; )
        {
            addLineToLog("Countdown " + i);
        }*/

        tableModel.addTableModelListener(e -> ZhJFtpForm.this.tidyUpTable());
    }


    private void startOrStop()
    {
        if ("Start".equals(this.cmdStart.getText()))
        {
            Credentials credentials = new Credentials();

            if (this.chkAnonymousReadOnly.isSelected())
            {
                credentials.addUser("anonymous", null, this.chkAnonymousReadWrite.isSelected());
            }

            for (int row = 0; row < this.tblUsers.getRowCount(); row++)
            {
                String username = this.tblUsers.getValueAt(row, 0).toString();
                String password = this.tblUsers.getValueAt(row, 1).toString();

                if (username != null
                    && password != null
                    && !username.equals("")
                    && !password.equals(""))
                {
                    credentials.addUser(username,
                        password,
                        (Boolean) this.tblUsers.getValueAt(row, 2));
                }
            }

            int port = (int) this.spnPort.getValue();

            JFtpServer.Options options = new JFtpServer.Options();

            options.verbose = true;

            this.serverThread
                = new ServerThread(
                    txtRoot.getText(),
                    port,
                    credentials,
                    options,
                    new ListOutputStream(this.lstLog));

            this.serverThread.start();

            this.cmdStart.setText("Stop");
        }
        else if ("Stop".equals(this.cmdStart.getText()))
        {
            this.serverThread.server.stopServer();
            this.serverThread.stop();

            this.cmdStart.setText("Start");
        }
        else
        {
            assert false : "Something went horribly wrong!";
        }
    }


    private boolean bitTidyingUpTable = false;

    private void tidyUpTable()
    {
        if (this.bitTidyingUpTable)
        {
            return;
        }

        this.bitTidyingUpTable = true;

        DefaultTableModel tableModel = (DefaultTableModel) this.tblUsers.getModel();

        int rowCount = this.tblUsers.getRowCount();

        for (int row = 0; row < rowCount; )
        {
            if ("".equals(this.tblUsers.getValueAt(row, 0)) && "".equals(this.tblUsers.getValueAt(row, 1)))
            {
                tableModel.removeRow(row);
                rowCount--;
            }
            else
            {
                row++;
            }
        }

        tableModel.addRow(new String[]{"", ""});

        this.bitTidyingUpTable = false;
    }


    public void addLineToLog(String line)
    {
        DefaultListModel<String> listModel = (DefaultListModel<String>) lstLog.getModel();
        listModel.addElement(line);
    }


    static ZhJFtpForm form;

    public static void main(String[] args)
    {
        JFrame frame = new JFrame("ZhJFtpForm");

        form = new ZhJFtpForm();
        frame.setContentPane(form.pnlMain);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
