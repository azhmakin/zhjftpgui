//
// Copyright (c) 2018 Andrey Zhmakin
//

package zhmakin.zhjftpgui;

import zhmakin.jftpserv.Credentials;
import zhmakin.jftpserv.JFtpServer;

import javax.swing.*;
import java.io.OutputStream;
import java.io.PrintStream;


/**
 * FTP server thread.
 * @author Andrey Zhmakin
 */
public class ServerThread
    extends Thread
{
    private String rootDir;
    private int port;
    private Credentials credentials;
    private JFtpServer.Options options;

    private OutputStream outputStream;

    JFtpServer server;


    public ServerThread(String              rootDir,
                        int                 port,
                        Credentials         credentials,
                        JFtpServer.Options  options,
                        OutputStream        outputStream )
    {
        this.rootDir = rootDir;
        this.port = port;
        this.credentials = credentials;
        this.options = options;
        this.outputStream = outputStream;
    }


    @Override
    public void run()
    {
        this.server = new JFtpServer(this.credentials, this.rootDir, this.port, this.options);

        this.server.setOutputStream(new PrintStream(this.outputStream));

        try
        {
            ZhJFtpForm.form.cmdStart.setText("Stop");
            this.server.runServer();
        }
        catch (Throwable e)
        {
            JOptionPane.showMessageDialog(ZhJFtpForm.form.pnlMain, e.getMessage(), "Error:", JOptionPane.ERROR_MESSAGE);
        }
        finally
        {
            ZhJFtpForm.form.cmdStart.setText("Start");
        }
    }
}
