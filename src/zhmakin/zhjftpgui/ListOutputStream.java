//
// Copyright (c) 2019 Andrey Zhmakin
//

package zhmakin.zhjftpgui;

import javax.swing.*;
import java.io.OutputStream;

/**
 * @author Andrey Zhmakin
 */
public class ListOutputStream
    extends OutputStream
{
    private StringBuilder line = new StringBuilder();
    private JList list;
    private int maxLines;


    public ListOutputStream(JList list)
    {
        this(list, 4096);
    }


    public ListOutputStream(JList list, int maxLines)
    {
        assert list.getModel() instanceof DefaultListModel : "List model should support insertion!";

        this.list = list;
        this.maxLines = maxLines;
    }


    @Override
    public synchronized void write(int b)
    {
        if (b == '\n')
        {
            this.flush();
        }
        else
        {
            this.line.append((char) b);
        }
    }


    @Override
    public synchronized void flush()
    {
        DefaultListModel<String> listModel = (DefaultListModel<String>) this.list.getModel();

        while (listModel.size() >= this.maxLines)
        {
            listModel.remove(0);
        }

        listModel.addElement(this.line.toString());

        this.line = new StringBuilder();
    }
}
